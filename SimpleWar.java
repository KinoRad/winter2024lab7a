public class SimpleWar{
	public static void main(String[] args){
		Deck d1 = new Deck();
		d1.shuffle();
		
		int player1Points = 0;
		int player2Points = 0;
		
		int round = 1;
		while(d1.getLenOfDeck() > 0){
			Card player1Card = d1.drawTopCard();
			Card player2Card = d1.drawTopCard();
			System.out.println("------------------------------------------");
			System.out.println("round " + round);
			System.out.println("Player 1 Card is: " + player1Card);
			System.out.println("Player 2 Card is: " + player2Card);
			System.out.println();
			if(player1Card.calculateScore() > player2Card.calculateScore()){
				player1Points++;
				System.out.println("Player 1 wins this round. They now have " + player1Points + " points");
				System.out.println("Player 2 has " + player2Points + " points");
			}else{
				player2Points++;
				System.out.println("Player 2 wins this round. They now have " + player2Points + " points");
				System.out.println("Player 1 has " + player1Points + " points");
			}
			System.out.println("------------------------------------------");
			round++;
		}
		if(player1Points > player2Points){
			System.out.println("Player 1 wins");
		}else if (player2Points > player1Points){
			System.out.println("Player 2 wins");
		}else{
			System.out.println("It's a draw!!");
		}
	}
}