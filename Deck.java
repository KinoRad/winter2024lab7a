import java.util.Random;
public class Deck{
	private int lenOfDeck;
	private Card[] stack;
	Random rng;
	
	public Deck(){
		rng = new Random();
		
	String [] suits = new String[] {"Hearts", "Spades", "Clubs", "Dimonds"};
		int[] values = new int[] {1,2,3,4,5,6,7,8,9,10,11,12,13}; 
		this.stack = new Card[suits.length * values.length];
		int i = 0;
		for(int value : values){
			for(String suit : suits){
				stack[i] = new Card(suit, value);
				i++;
			}
		}
		this.lenOfDeck = suits.length * values.length;
	}
	
	public int getLenOfDeck(){
		return lenOfDeck;
	}
	
	public void shuffle(){
		for(int i = 0; i < lenOfDeck; i++){
			int ranNum = rng.nextInt(lenOfDeck);
			Card placholder = stack[i];
			stack[i] = stack[ranNum];
			stack[ranNum] = placholder;
		}
	}
	
	public String toString(){
		String outPut = "";
		for(Card c : stack){
			outPut = outPut + c +" \n";
		}
		return outPut;
	}
	
	public Card drawTopCard(){
		Card lastCard = stack[lenOfDeck - 1];
		Card[] placholder = stack;
		this.lenOfDeck--;
		this.stack = new Card[lenOfDeck];
		for(int i = 0; i < lenOfDeck; i++){
			this.stack[i] = placholder[i];
		}
		return lastCard;
	}
}